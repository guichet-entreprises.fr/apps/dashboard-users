
# Changelog
Ce fichier contient les modifications techniques du module.

## [2.16.1.0] - 2020-09-11

###  Ajout

- Modifier le logo de la bannière

__Properties__ :

### Modification

| fichier   |      nom      | nouvelle valeur |
|-----------|:-------------:|----------------:|
| ge-dashboard-webapp-application.properties | ui.theme.default | default |

## [2.11.12.0] - 2019-09-06

__Properties__ :

### Ajout

| fichier   |      nom      |  description | valeur |
|-----------|:-------------:|-------------:|-------:|
| ge-dashboard-webapp-links.properties | app.link.4.welcome | URL publique de Welcome Guichet-Entreprises | The available services&#124;https://welcome.${env}.guichet-entreprises.fr |

## [2.11.10.0] - 2019-08-07

### Modification
-Moderniser la page de consultation des dossiers MINE-890

## [2.11.9.0] - 2019-07-31

### Modification
-Moderniser la page de consultation des dossiers MINE-890

## [2.11.7.1] - 2019-07-11

- Projet : [dashboard-webclient](https://tools.projet-ge.fr/gitlab/sonic/ge-dashboard-webclient)

###  Ajout

- Modifier la page d'accueil du tableau de bord

__Properties__ :

| fichier   |      nom      |  description | valeur |
|-----------|:-------------:|-------------:|-------:|
| ge-dashboard-webapp-links.properties | app.link.2.dashboard | URL publique du tableau de bord des déclarants | My records&#124;https://dashboard.${env}.guichet-entreprises.fr |
| ge-dashboard-webapp-links.properties | app.link.3.profiler | URL publique de la formalité profiler de routage | Create a new record&#124;https://profiler.${env}.guichet-entreprises.fr/form/use?reference=Profiler%2FRoutage |

### Modification

### Suppression

| fichier   |      nom      |  description | valeur |
|-----------|:-------------:|-------------:|-------:|
| ge-dashboard-webapp-links.properties | app.link.2.profiler | URL publique de la formalité profiler de routage | Create a new record&#124;https://profiler.${env}.guichet-entreprises.fr/form/use?reference=Profiler%2FRoutage |


## [2.11.7.0] - 2019-07-11

- Projet : [dashboard-webclient](https://tools.projet-ge.fr/gitlab/sonic/ge-dashboard-webclient)

###  Ajout
- Modifier la page d'accueil du tableau de bord


### Modification

| fichier   |      nom      |  description | valeur |
|-----------|:-------------:|-------------:|-------:|
| ge-dashboard-webapp-application.properties | profilerUrl | URL publique de la formalité profiler de routage | https://profiler.${env}.guichet-entreprises.fr/form/use?reference=Profiler%2FRoutage |

### Suppression


## [2.11.6.0] - 2019-07-03

- Projet : [dashboard-webclient](https://tools.projet-ge.fr/gitlab/sonic/ge-dashboard-webclient)

###  Ajout
- Déplacer les liens dans le bandeau bleu

__Properties__ :

- Ajout d'un nouveau fichier "ge-dashboard-webapp-links.properties"

| fichier   |      nom      |  description | valeur |
|-----------|:-------------:|-------------:|-------:|
| ge-dashboard-webapp-links.properties | app.link.1.home | URL publique du www côté Guichet-Entreprises | Home&#124;https://www.${env}.guichet-entreprises.fr |
| ge-dashboard-webapp-links.properties | app.link.2.profiler | URL publique de la formalité profiler de routage | Create a new record&#124;https://profiler.${env}.guichet-entreprises.fr/form/use?reference=Profiler%2FRoutage |

### Modification


### Suppression


## [2.11.2.1] - 2019-05-09

- Projet : [dashboard-webclient](https://tools.projet-ge.fr/gitlab/sonic/ge-dashboard-webclient)

###  Ajout
- Popup pour l'affichage de l'historique tracker d'un dossier
- __Properties__ :

- Ajout d'un nouveau fichier "ge-dashboard-webapp-tracker.properties"
| fichier   |      nom      |  description | valeur |
|-----------|:-------------:|-------------:|-------:|
| ge-dashboard-webapp-tracker.properties | mas.tracker.service.url | URL privé pour récupérer l'historique tracker | http://\{VH_TRACKER_SERVER}/api |

### Modification


### Suppression


