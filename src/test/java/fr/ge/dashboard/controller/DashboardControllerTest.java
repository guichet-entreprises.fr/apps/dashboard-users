/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.dashboard.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.stream.Collectors;

import javax.ws.rs.core.Response;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import configuration.mock.DashboardUserBeanMock;
import fr.ge.common.nash.ws.v1.bean.RecordInfoBean;
import fr.ge.common.nash.ws.v1.service.IRecordService;
import fr.ge.core.exception.TechniqueException;
import fr.ge.ct.authentification.bean.AccountUserBean;
import fr.ge.ct.authentification.facade.IAccountFacade;
import fr.ge.dashboard.bean.DashboardUserBean;
import fr.ge.dashboard.context.DashboardThreadContext;
import fr.ge.dashboard.facade.IRecordFacade;
import fr.ge.gent.forms.ws.v1.service.IFormsRestService;
import fr.ge.record.ws.v1.model.LocalizedLabel;
import fr.ge.record.ws.v1.model.LocalizedString;
import fr.ge.record.ws.v1.model.RecordAction;
import fr.ge.record.ws.v1.model.RecordActionTypeEnum;
import fr.ge.record.ws.v1.model.RecordDisplay;
import fr.ge.record.ws.v1.model.RecordResult;
import fr.ge.record.ws.v1.rest.IRecordDisplayRestService;
import fr.ge.tracker.bean.Message;
import fr.ge.tracker.bean.UidNfo;
import fr.ge.tracker.service.v1.IUidRestService;

/**
 * Class test for Dashboard controller.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/applicationContext-ge-dashboard-webapp-config.xml", "/spring/test-client-context.xml" })
@WebAppConfiguration
public class DashboardControllerTest {

    private static final String RECORD_UID = "2019-03-REC-ORD-42";

    /** Forms facade. **/
    @Autowired
    private IRecordFacade recordFacade;

    /** Interface of the Web Service Forms. */
    @Autowired
    private IRecordDisplayRestService recordFormsRestService;

    /** Interface of the Service Forms. */
    @Autowired
    private IFormsRestService formsRestService;

    /** Interface of the Web Service Nash. */
    @Autowired
    private IRecordDisplayRestService recordNashRestService;

    /** interface pour la façade Account. */
    @Autowired
    private IAccountFacade accountFacade;

    /** Record service from profiler. **/
    @Autowired
    private IRecordService profilerRecordService;

    /** Record service from profiler. **/
    @Autowired
    private IRecordService nashRecordService;

    /** the main object */
    private MockMvc mvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    /** Interface of the tracker Service. */
    @Autowired
    private IUidRestService trackerService;

    /**
     * Injection of mocks in controller.
     *
     * @throws Exception
     *             exception
     */
    @Before
    public void setup() throws Exception {
        this.mvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
        reset(this.recordFacade, this.accountFacade, this.recordFormsRestService, this.recordNashRestService, this.profilerRecordService, this.nashRecordService);
        DashboardUserBeanMock.init("1");
    }

    /**
     * Test display home page.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testDisplayHome() throws Exception {
        this.mvc.perform(get("/")).andExpect(redirectedUrl("/search"));
    }

    /**
     * Test display records from guichet-entreprises.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testDisplayRecordsGE() throws Exception {
        final AccountUserBean geUserBean = new AccountUserBean();
        geUserBean.setConnectionDate(LocalDateTime.now().toString());

        Mockito.when(this.accountFacade.readUser(any(String.class))).thenReturn(geUserBean);

        this.mvc.perform(get("/search").header("Host", "dashboard.dev.guichet-entreprises.fr").param("searchTerms", "searchText").param("sort", "creation")) //
                .andExpect(status().isOk()) //
                .andExpect(view().name("dashboard/dashboard"));

    }

    @Test
    public void testSearchPost() throws Exception {
        Mockito.when(this.accountFacade.readUser(any(String.class))).thenReturn(new AccountUserBean());

        this.mvc.perform(post("/search").header("Host", "dashboard.dev.guichet-entreprises.fr").param("searchTerms", "searchText").param("sort", "creation")) //
                .andExpect(status().isOk()) //
                .andExpect(view().name("dashboard/dashboard"));
    }

    /**
     * Test display records from guichet-entreprises.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testSearchRecordsGE() throws Exception {
        final RecordResult recordResult = this.buildRecordResultGE();
        final AccountUserBean geUserBean = new AccountUserBean();
        geUserBean.setConnectionDate(LocalDateTime.now().toString());

        when(this.recordFormsRestService.searchRecords(any(String.class), any(int.class), any(int.class), any(String.class), any(String.class))).thenReturn(recordResult);
        when(this.accountFacade.readUser(any(String.class))).thenReturn(geUserBean);

        this.mvc.perform(get("/search").header("Host", "dashboard.dev.guichet-entreprises.fr").param("searchTerms", "searchText").param("sort", "creation")) //
                .andExpect(status().isOk()) //
                .andExpect(view().name("dashboard/dashboard"));
    }

    /**
     * Test display records from guichet-entreprises with an exception in WS ->
     * no interruption of the service.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testDisplayRecordsGEException() throws Exception {
        final AccountUserBean geUserBean = new AccountUserBean();
        geUserBean.setConnectionDate(LocalDateTime.now().toString());

        Mockito.when(this.accountFacade.readUser(any(String.class))).thenReturn(geUserBean);

        this.mvc.perform(get("/search").header("Host", "dashboard.dev.guichet-entreprises.fr").param("searchTerms", "searchText").param("sort", "creation")) //
                .andExpect(status().isOk()) //
                .andExpect(view().name("dashboard/dashboard"));
    }

    /**
     * Test display records from guichet-entreprises with an exception in WS ->
     * no interruption of the service.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testSearchRecordsGEException() throws Exception {
        final AccountUserBean geUserBean = new AccountUserBean();
        geUserBean.setConnectionDate(LocalDateTime.now().toString());

        Mockito.when(this.recordFacade.searchGERecords(any(String.class), any(int.class), any(int.class), any(String.class), any(String.class))).thenThrow(TechniqueException.class);
        Mockito.when(this.accountFacade.readUser(any(String.class))).thenReturn(geUserBean);

        this.mvc.perform(get("/search").header("Host", "dashboard.dev.guichet-entreprises.fr").param("searchTerms", "searchText").param("sort", "creation")) //
                .andExpect(status().isOk()) //
                .andExpect(view().name("dashboard/dashboard"));
    }

    /**
     * Test display records from guichet-qualifications.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testDisplayRecordsGQ() throws Exception {
        final AccountUserBean geUserBean = new AccountUserBean();
        geUserBean.setConnectionDate(LocalDateTime.now().toString());

        Mockito.when(this.accountFacade.readUser(any(String.class))).thenReturn(geUserBean);

        this.mvc.perform(get("/search").header("Host", "dashboard.dev.guichet-qualifications.fr").param("searchTerms", "searchText").param("sort", "creation")) //
                .andExpect(status().isOk()) //
                .andExpect(view().name("dashboard/dashboard"));
    }

    /**
     * @return
     */
    private RecordResult constructRecordResultGQ() {
        final RecordAction action = new RecordAction();
        action.setLabel(this.buildLocalizedLabel("History", "en", "fr"));
        action.setType(RecordActionTypeEnum.HISTORY);
        action.setUri("");

        final RecordDisplay record = new RecordDisplay();
        record.setAuthorId("2");
        record.setActions(Arrays.asList(action));

        final RecordResult recordResult = new RecordResult();
        recordResult.setRecords(Arrays.asList(record));
        recordResult.setTotalResults("1");
        recordResult.setStartIndex("0");
        recordResult.setNbResults("10");

        return recordResult;
    }

    /**
     * Test display records from guichet-qualifications.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testSearchRecordsGQ() throws Exception {
        final RecordResult recordResult = this.constructRecordResultGQ();
        final AccountUserBean geUserBean = new AccountUserBean();
        geUserBean.setConnectionDate(LocalDateTime.now().toString());

        Mockito.when(this.recordFacade.searchGQRecords(any(String.class), any(int.class), any(int.class), any(String.class), any(String.class))).thenReturn(recordResult);
        Mockito.when(this.accountFacade.readUser(any(String.class))).thenReturn(geUserBean);

        this.mvc.perform(get("/search").header("Host", "dashboard.dev.guichet-qualifications.fr").param("searchTerms", "searchText").param("sort", "creation")) //
                .andExpect(status().isOk()) //
                .andExpect(view().name("dashboard/dashboard"));
    }

    /**
     * Test GetUserInformation method.
     *
     * @throws Exception
     */
    // @Test
    // public void testGetUserInformation() throws Exception {
    // final Model model = Mockito.mock(Model.class);
    // final MockHttpServletRequest request = new MockHttpServletRequest();
    // request.setServerName("dashboard.dev.guichet-entreprises.fr");
    // request.setRequestURI("/record");
    // request.setRemoteHost("dashboard.dev.guichet-entreprises.fr");
    // final Locale locale = Locale.getDefault();
    //
    // final AccountUserBean geUserBean = new AccountUserBean();
    // geUserBean.setConnectionDate(LocalDateTime.now().toString());
    // Mockito.when(this.accountFacade.readUser(any(String.class))).thenReturn(geUserBean);
    //
    // final DashboardUserBean userInformation =
    // this.dashboardController.getUserInformation(model, locale);
    // Assert.assertNotNull(userInformation);
    // }

    /**
     * Test display records from profiler.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testDisplayRecordsProfiler() throws Exception {
        final LocalDateTime now = LocalDateTime.now();
        final AccountUserBean geUserBean = new AccountUserBean();
        geUserBean.setConnectionDate(now.toString());

        when(this.accountFacade.readUser(any(String.class))).thenReturn(geUserBean);
        when(this.profilerRecordService.assign(any(String.class), any(String.class))).thenReturn(Response.ok().build());

        this.mvc.perform(get("/search").header("Host", "dashboard.dev.guichet-entreprises.fr").param("searchTerms", "searchText").param("sort", "creation")) //
                .andExpect(status().isOk()) //
                .andExpect(view().name("dashboard/dashboard"));

    }

    @Test
    public void testRemoveOk() throws Exception {
        when(this.nashRecordService.remove(any(String.class))).thenReturn(Response.ok().build());
        this.mvc.perform(get("/record/{code}/remove", RECORD_UID)) //
                .andExpect(status().isOk());
    }

    @Test
    public void testRemoveKo() throws Exception {
        when(this.nashRecordService.remove(any(String.class))).thenReturn(Response.serverError().build());
        this.mvc.perform(get("/record/{code}/remove", RECORD_UID)) //
                .andExpect(status().is5xxServerError());
    }

    private RecordResult buildRecordResultGE() {
        final RecordResult recordResult = new RecordResult();

        final RecordAction action = new RecordAction();
        action.setLabel(this.buildLocalizedLabel("History", "en", "fr"));
        action.setType(RecordActionTypeEnum.HISTORY);
        action.setUri("");

        final RecordDisplay record = new RecordDisplay();
        record.setAuthorId("2");
        record.setActions(Arrays.asList(action));
        record.setSummary(this.buildLocalizedLabel("Summary", "en", "fr"));
        record.setType(this.buildLocalizedLabel("Preparation", "en", "fr"));
        record.setProgress("33");

        recordResult.setRecords(Arrays.asList(record));
        recordResult.setTotalResults("1");
        recordResult.setStartIndex("0");
        recordResult.setNbResults("10");

        return recordResult;
    }

    @Test
    public void testSearchOrder() throws Exception {
        final String[] searchOrders = new String[] { null, "creation", "update", "title" };
        Mockito.when(this.accountFacade.readUser(any(String.class))).thenReturn(new AccountUserBean());

        for (final String order : searchOrders) {
            this.mvc.perform(post("/search").header("Host", "dashboard.dev.guichet-entreprises.fr").param("sort", order)) //
                    .andExpect(status().isOk()) //
                    .andExpect(view().name("dashboard/dashboard"));
        }
    }

    private LocalizedLabel buildLocalizedLabel(final String label, final String... locales) {
        final LocalizedLabel localizedLabel = new LocalizedLabel();
        localizedLabel.setActualLabel("Default " + label);
        localizedLabel.setDefaultLocale(locales[0]);
        localizedLabel.setValues(Arrays.stream(locales).map(locale -> new LocalizedString(locale, locale.toUpperCase() + " " + label)).collect(Collectors.toList()));

        return localizedLabel;
    }

    @Test
    public void testHistoryFromNash() throws Exception {
        // on verifie que le context
        DashboardThreadContext.setUser(new DashboardUserBean("1"));

        final RecordInfoBean recordInfo = mock(RecordInfoBean.class);
        when(recordInfo.getAuthor()).thenReturn("1");
        when(this.nashRecordService.load(anyString())).thenReturn(recordInfo);
        UidNfo nfo = mock(UidNfo.class);
        when(nfo.getMessages()).thenReturn(Arrays.asList(new Message()));
        when(this.trackerService.getUid(anyString())).thenReturn(nfo);

        this.mvc.perform(get("/record/{code}/history", RECORD_UID)) //
                .andExpect(status().is2xxSuccessful());
    }

    @Test
    public void testHistoryFromForms() throws Exception {
        // on verifie que le context
        DashboardThreadContext.setUser(new DashboardUserBean("1"));

        final RecordInfoBean recordInfo = mock(RecordInfoBean.class);
        when(recordInfo.getAuthor()).thenReturn("1");
        when(this.formsRestService.own(anyString(), anyString())).thenReturn(true);
        UidNfo nfo = mock(UidNfo.class);
        when(nfo.getMessages()).thenReturn(Arrays.asList(new Message()));
        when(this.trackerService.getUid(anyString())).thenReturn(nfo);

        this.mvc.perform(get("/record/{code}/history", "M78010000750")) //
                .andExpect(status().is2xxSuccessful());
    }
}
