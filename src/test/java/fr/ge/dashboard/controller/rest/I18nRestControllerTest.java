package fr.ge.dashboard.controller.rest;

import static org.hamcrest.Matchers.startsWith;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Locale;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/applicationContext-ge-dashboard-webapp-config.xml", "/spring/test-client-context.xml" })
@WebAppConfiguration
public class I18nRestControllerTest {

    /** the main object */
    private MockMvc mvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    /**
     * Set up.
     *
     * @throws Exception
     *             exception
     */
    @Before
    public void setUp() throws Exception {
        this.mvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
    }

    /**
     * Test bundle.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testBundle() throws Exception {
        this.mvc.perform(get("/rest/i18n/bundle.js").locale(Locale.JAPANESE)) //
                .andExpect(status().is(HttpStatus.OK.value())) //
                .andExpect(header().string("Content-Type", "text/javascript")) //
                .andExpect(content().string(startsWith("requirejs(['lib/i18n'], function(i18n) {\ni18n.register('" + Locale.JAPANESE.getLanguage() + "', {")));
    }

}
