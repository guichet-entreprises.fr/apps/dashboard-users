package fr.ge.dashboard.facade.impl;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import configuration.mock.DashboardUserBeanMock;
import fr.ge.core.exception.FunctionalException;
import fr.ge.core.exception.TechniqueException;
import fr.ge.record.ws.v1.model.RecordResult;
import fr.ge.record.ws.v1.rest.IRecordDisplayRestService;

public class RecordFacadeImplTest {
  /** Record facade */
  @InjectMocks
  private RecordFacadeImpl recordFacade;

  /** Interface of the Web Service Forms. */
  @Mock
  private IRecordDisplayRestService recordFormsRestService;

  /** Interface of the Web Service Nash. */
  @Mock
  private IRecordDisplayRestService recordNashRestService;

  /**
   * Injection of mocks in controller.
   *
   * @throws Exception
   *           exception
   */
  @Before
  public void setup() throws Exception {
    MockitoAnnotations.initMocks(this);
    DashboardUserBeanMock.init("1");
  }

  /**
   * Test searchGERecords case Ok.
   * 
   * @throws TechniqueException
   * @throws FunctionalException
   */
  @Test
  public void searchGERecordsTestOk() throws TechniqueException, FunctionalException {
    RecordResult recordResult = new RecordResult();
    recordResult.setStartIndex("0");
    recordResult.setNbResults("-1");
    recordResult.setTotalResults("1");

    Mockito.when(recordFormsRestService.searchRecords(Matchers.anyString(), Matchers.anyInt(), Matchers.anyInt(),
      Matchers.anyString(), Matchers.anyString())).thenReturn(recordResult);

    RecordResult recoldResultExpected = recordFacade.searchGERecords("2", 0, -1, "", "searchText");
    Assert.assertNotNull(recoldResultExpected);
  }

  /**
   * Test searchGERecords case Ok.
   * 
   * @throws TechniqueException
   * @throws FunctionalException
   */
  @Test
  public void searchGQRecordsTestOk() throws TechniqueException, FunctionalException {
    RecordResult recordResult = new RecordResult();
    recordResult.setStartIndex("0");
    recordResult.setNbResults("-1");
    recordResult.setTotalResults("1");

    Mockito.when(recordNashRestService.searchRecords(Matchers.anyString(), Matchers.anyInt(), Matchers.anyInt(),
      Matchers.anyString(), Matchers.anyString())).thenReturn(recordResult);

    RecordResult recoldResultExpected = recordFacade.searchGQRecords("2", 0, -1, "", "searchText");
    Assert.assertNotNull(recoldResultExpected);
  }

  /**
   * Test searchGERecords case Web Service error.
   * 
   * @throws TechniqueException
   * @throws FunctionalException
   * 
   */
  @Test(expected = TechniqueException.class)
  public void searchGERecordsTestServerError() throws TechniqueException, FunctionalException {
    Mockito.when(recordNashRestService.searchRecords(Matchers.anyString(), Matchers.anyInt(), Matchers.anyInt(),
      Matchers.anyString(), Matchers.anyString())).thenThrow(new NullPointerException());

    recordFacade.searchGERecords("2", 0, -1, "", "searchText");
  }

  /**
   * Test searchGQRecords case Web Service error.
   * 
   * @throws TechniqueException
   * @throws FunctionalException
   * 
   */
  @Test(expected = TechniqueException.class)
  public void searchGQRecordsTestServerError() throws TechniqueException, FunctionalException {
    Mockito.when(recordNashRestService.searchRecords(Matchers.anyString(), Matchers.anyInt(), Matchers.anyInt(),
      Matchers.anyString(), Matchers.anyString())).thenThrow(new NullPointerException());

    recordFacade.searchGQRecords("2", 0, -1, "", "searchText");
  }

  /**
   * Test searchGERecords case result null.
   * 
   * @throws FunctionalException
   * 
   */
  @Test(expected = TechniqueException.class)
  public void searchGERecordsTestResultNull() throws TechniqueException, FunctionalException {
    Mockito.when(recordFormsRestService.searchRecords(Matchers.anyString(), Matchers.anyInt(), Matchers.anyInt(),
      Matchers.anyString(), Matchers.anyString())).thenReturn(null);

    recordFacade.searchGERecords("2", 0, -1, "", "searchText");
  }

  /**
   * Test searchGERecords case result null.
   * 
   * @throws FunctionalException
   * 
   */
  @Test(expected = TechniqueException.class)
  public void searchGQRecordsTestResultNull() throws TechniqueException, FunctionalException {
    Mockito.when(recordNashRestService.searchRecords(Matchers.anyString(), Matchers.anyInt(), Matchers.anyInt(),
      Matchers.anyString(), Matchers.anyString())).thenReturn(null);

    recordFacade.searchGQRecords("2", 0, -1, "", "searchText");
  }

}
