/**
 * 
 */
package configuration.mock;

import fr.ge.dashboard.bean.DashboardUserBean;
import fr.ge.dashboard.context.DashboardThreadContext;

/**
 * Mock pour DashboardUserBean.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public final class DashboardUserBeanMock {

  /**
   * Constructeur de la classe.
   *
   */
  private DashboardUserBeanMock() {
    super();
  }

  /**
   * Methode pour mocker facilement un utilisateur en ne passant que l'identifiant.
   * 
   * @param idUtilisateur
   *          : Identifiant de l'utilisateur
   */
  public static void init(final String idUtilisateur) {
    DashboardUserBean dashboardUserBean = new DashboardUserBean(idUtilisateur);
    DashboardThreadContext.setUser(dashboardUserBean);
  }

  /**
   * Methode pour mocker facilement un utilisateur.
   * 
   * @param idUtilisateur
   *          : Identifiant de l'utilisateur
   * @param nom
   *          : nom de l'utilisateur
   * @param prenom
   *          : prenom de l'utilisateur
   */
  public static void init(final String idUtilisateur, final String nom, final String prenom, final String civilite,
    final String email, final String origine, final Boolean validPhone) {
    DashboardUserBean dashboardUserBean = new DashboardUserBean(idUtilisateur);
    dashboardUserBean.setNom(nom);
    dashboardUserBean.setPrenom(prenom);
    DashboardThreadContext.setUser(dashboardUserBean);
  }
}
