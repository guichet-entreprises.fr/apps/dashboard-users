package configuration.mock;

import static org.fest.assertions.Assertions.assertThat;

import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;

import org.junit.Test;

import fr.ge.dashboard.bean.DashboardUserBean;
import fr.ge.dashboard.context.DashboardThreadContext;

public class DashboardThreadContextTest {

  /**
   * La constante thread local.
   */
  public static final ThreadLocal < DashboardUserBean > LOCAL_USER = new ThreadLocal < DashboardUserBean >();

  /**
   * Methode de test de l'attribut de la class.
   * 
   * @throws Exception
   *           : Exception
   */
  @Test
  public void classTest() throws Exception {
    final Constructor < ? > constructor = DashboardThreadContext.class.getDeclaredConstructor();
    assertThat(constructor.isAccessible()).isFalse();
    assertThat(Modifier.isPrivate(constructor.getModifiers())).isTrue();
    constructor.setAccessible(true);
    constructor.newInstance();
    assertThat(constructor.isAccessible()).isTrue();
    constructor.setAccessible(false);
  }

  /**
   * Methode de test global pour GentThreadContext.
   *
   */
  @Test
  public void initTest() {

    // on verifie que le context
    DashboardUserBean userFormsBean = new DashboardUserBean("1");
    DashboardThreadContext.setUser(userFormsBean);

    assertThat(DashboardThreadContext.getUser().getId()).isEqualTo("1");

    // on verifie qu'il est bien supprime du contexte
    DashboardThreadContext.unsetUser();
    assertThat(DashboardThreadContext.getUser()).isNull();
  }

}
