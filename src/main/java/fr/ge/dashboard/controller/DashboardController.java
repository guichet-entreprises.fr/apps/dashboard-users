/**
 *
 */
package fr.ge.dashboard.controller;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import fr.ge.common.nash.ws.v1.bean.RecordInfoBean;
import fr.ge.common.nash.ws.v1.service.IRecordService;
import fr.ge.common.utils.property.LinkPropertyPlaceholder;
import fr.ge.core.exception.FunctionalException;
import fr.ge.core.exception.TechniqueException;
import fr.ge.core.log.GestionnaireTrace;
import fr.ge.ct.authentification.bean.AccountUserBean;
import fr.ge.ct.authentification.facade.IAccountFacade;
import fr.ge.dashboard.bean.DashboardRecordDisplay;
import fr.ge.dashboard.bean.DashboardRecordResult;
import fr.ge.dashboard.bean.DashboardUserBean;
import fr.ge.dashboard.bean.conf.DashboardConfigurationWebBean;
import fr.ge.dashboard.constant.ModulesDashboardEnum;
import fr.ge.dashboard.context.DashboardThreadContext;
import fr.ge.gent.forms.ws.v1.service.IFormsRestService;
import fr.ge.record.ws.v1.model.LocalizedLabel;
import fr.ge.record.ws.v1.model.LocalizedString;
import fr.ge.record.ws.v1.model.RecordAction;
import fr.ge.record.ws.v1.model.RecordDisplay;
import fr.ge.record.ws.v1.model.RecordResult;
import fr.ge.record.ws.v1.model.RecordSortEnum;
import fr.ge.record.ws.v1.rest.IRecordDisplayRestService;
import fr.ge.tracker.bean.Message;
import fr.ge.tracker.service.v1.IUidRestService;

/**
 * Dashboard main page controller.
 *
 * @author $Author: amonsone $
 * @version $Revision: 0 $
 */
@Controller
public class DashboardController {

    private static final DateTimeFormatter FORMATTER_TIME = DateTimeFormatter.ofPattern("HH:mm");

    private static final DateTimeFormatter FORMATTER_DATE = DateTimeFormatter.ofPattern("dd/MM/yyyy");

    /** Interface of the Web Service Forms. */
    @Autowired
    private IRecordDisplayRestService recordFormsRestService;

    /** Interface of the Web Service Nash. */
    @Autowired
    private IRecordDisplayRestService recordNashRestService;

    /** Interface of the record Service Nash. */
    @Autowired
    private IRecordService nashRecordService;

    /** Account facade. */
    @Autowired
    private IAccountFacade accountFacade;

    /** Dashboard configuration bean. */
    @Autowired
    private DashboardConfigurationWebBean dashboardConfigurationWebBean;

    /** Interface of the tracker Service. */
    @Autowired
    private IUidRestService trackerService;

    /** Interface of the forms Service. */
    @Autowired
    private IFormsRestService formsRestService;

    /** The functionnal logger. */
    private static final Logger LOGGER_FONC = GestionnaireTrace.getLoggerFonctionnel();

    /** The technical logger. */
    private static final Logger LOGGER_TECH = GestionnaireTrace.getLoggerTechnique();

    private static String DEFAULT_PREFIX_AUTHOR = "GE/";

    /** La constante NASH_RECORD_IDENTIFIER. */
    private static final Pattern NASH_RECORD_IDENTIFIER = Pattern.compile("[0-9]{4}\\-[0-9]{2}\\-[A-Z]{3}\\-[A-Z]{3}\\-[0-9]{2}");

    @Autowired
    @Qualifier("linksPlaceholderConfigurer")
    private LinkPropertyPlaceholder linkPlaceholder;

    /**
     * Display dashboard home page.
     *
     * @return string
     */
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String displayHome() {
        return "redirect:/search";
    }

    /**
     * Display all the user's records according to searching text.
     *
     * @param model
     *            The model
     * @param request
     *            The HTTP request
     * @param locale
     *            The browser locale
     * @param searchTerms
     *            The searching text
     * @return The page to display
     * @throws TechniqueException
     *             The technical exception
     * @throws FunctionalException
     */
    @RequestMapping(value = "/search", method = RequestMethod.POST)
    public String searchRecords(final Model model, final HttpServletRequest request, final Locale locale, @ModelAttribute("searchTerms") final String searchTerms,
            @ModelAttribute("sort") final String sort) throws TechniqueException, FunctionalException {
        return this.searchRecordsAndSort(model, request, locale, searchTerms, sort);
    }

    /**
     * Display all the user's records according to searching text.
     *
     * @param model
     *            The model
     * @param request
     *            The HTTP request
     * @param locale
     *            The browser locale
     * @param searchTerms
     *            The searching text
     * @return The page to display
     * @throws TechniqueException
     *             The technical exception.
     * @throws FunctionalException
     */
    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public String initRecords(final Model model, final HttpServletRequest request, final Locale locale, @QueryParam("searchTerms") final String searchTerms, @QueryParam("sort") final String sort)
            throws TechniqueException, FunctionalException {
        model.addAttribute("dashboardLinks", this.linkPlaceholder.getProps());
        model.addAttribute("user", DashboardThreadContext.getUser());
        return this.searchRecordsAndSort(model, request, locale, searchTerms, sort);
    }

    /**
     * Removes a record.
     *
     * @param code
     * @return ResponseEntity
     */
    @GetMapping("/record/{code:[0-9]{4}-[0-9]{2}-[A-Z]{3}-[A-Z]{3}-[0-9]{2}}/remove")
    public ResponseEntity<Response.Status> removeRecord(@PathVariable("code") final String code) {
        if (this.nashRecordService.remove(code).getStatus() == Response.Status.OK.getStatusCode()) {
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    /**
     * Show Tracker History
     *
     * @param code
     * @return ResponseEntity
     */
    @GetMapping("/record/{code}/history")
    public ResponseEntity<List<Message>> showTrackerHistory(@PathVariable("code") final String code, final Model model, final Locale locale) {
        LOGGER_FONC.info("Get record history");

        // TODO Use own nash service
        if (NASH_RECORD_IDENTIFIER.matcher(code).matches()) {
            final String author = StringUtils.join(DEFAULT_PREFIX_AUTHOR, DashboardThreadContext.getUser().getId());
            final RecordInfoBean recordInfo = this.nashRecordService.load(code);
            if (null == recordInfo || StringUtils.isEmpty(recordInfo.getAuthor()) && !author.equals(recordInfo.getAuthor())) {
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            if (!this.formsRestService.own("DOSSIER_" + code, DashboardThreadContext.getUser().getId())) {
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<>(this.trackerService.getUid(code).getMessages(), HttpStatus.OK);
    }

    /**
     * Method to search Records and sort them.
     *
     * @param model
     * @param request
     * @param locale
     * @param searchTerms
     * @param sort
     * @return
     * @throws FunctionalException
     * @throws TechniqueException
     */
    private String searchRecordsAndSort(final Model model, final HttpServletRequest request, final Locale locale, final String searchTerms, String sort)
            throws FunctionalException, TechniqueException {

        LOGGER_FONC.info("Search GE/GQ records");

        if (StringUtils.isEmpty(sort)) {
            sort = RecordSortEnum.CREATION.getName();
        }
        model.addAttribute("sort", sort);
        if (null != searchTerms) {
            model.addAttribute("searchTerms", searchTerms);
        }

        final DashboardRecordResult dashboardRecordResult = new DashboardRecordResult();

        // -->searching for records from GE/Forms (FORMS 1)
        // this.searchRecordsFromModule(dashboardRecordResult,
        // ModulesDashboardEnum.GE, locale, searchTerms, sort);

        // -->searching for records from GQ/NASH
        this.searchRecordsFromModule(dashboardRecordResult, ModulesDashboardEnum.GQ, locale, searchTerms, sort);

        // sort
        if (RecordSortEnum.CREATION.getName().equals(sort)) {
            dashboardRecordResult.getRecords().sort((o1, o2) -> o2.getRecordDisplay().getCreationDate().compareTo(o1.getRecordDisplay().getCreationDate()));
        } else if (RecordSortEnum.UPDATE.getName().equals(sort)) {
            dashboardRecordResult.getRecords().sort((o1, o2) -> o2.getRecordDisplay().getUpdateDate().compareTo(o1.getRecordDisplay().getUpdateDate()));
        } else if (RecordSortEnum.TITLE.getName().equals(sort)) {
            dashboardRecordResult.getRecords().sort((o1, o2) -> o1.getRecordDisplay().getTitle().compareTo(o2.getRecordDisplay().getTitle()));
        }

        dashboardRecordResult.setTotalResults(dashboardRecordResult.getRecords().size());
        dashboardRecordResult.setOneDatasourceInError(dashboardRecordResult.getNbDatasourcesOk() < 2);
        dashboardRecordResult.setAllDatasourcesInError(dashboardRecordResult.getNbDatasourcesOk() == 0);
        model.addAttribute("recordResult", dashboardRecordResult);

        // ajout des URL externes Forms/Nash
        this.buildExternalUrl(model);

        // affichage HTML en fonction de l'host de l'URL
        return this.displaySkin(model, request);

    }

    /**
     * Method to display GE or GQ skin.
     *
     * @param model
     *            The Spring model
     * @param request
     *            The HTTP request
     * @return The skin to display
     * @throws UnsupportedEncodingException
     */
    private String displaySkin(final Model model, final HttpServletRequest request) {
        LOGGER_FONC.info("Determine skin to display");
        final String hostName = this.getHost(request);
        final String currentUrl = request.getRequestURL().toString();
        final String urlFront = this.dashboardConfigurationWebBean.getUrlFront(hostName);
        final String urlModificationCallback = this.dashboardConfigurationWebBean.getUrlModification(hostName, currentUrl);

        model.addAttribute("urlFrontGQ", this.dashboardConfigurationWebBean.getUrlFrontGQ());
        model.addAttribute("urlFront", urlFront);
        model.addAttribute("urlModification", urlModificationCallback);

        if (null != hostName && hostName.contains(this.dashboardConfigurationWebBean.getGqDomainName())) {
            LOGGER_FONC.debug("Affichage Dashboard Guichet-Qualifications.fr");
            model.addAttribute("urlLogout", this.dashboardConfigurationWebBean.getUrlLogoutGQ());
        } else {
            LOGGER_FONC.debug("Affichage Dashboard Guichet-Entreprises.fr");
            model.addAttribute("urlLogout", this.dashboardConfigurationWebBean.getUrlLogoutGE());
        }

        return "dashboard/dashboard";
    }

    /**
     * Build urls to display into the GE/GQ page.
     *
     * @param model
     *            The Spring model
     */
    private void buildExternalUrl(final Model model) {
        LOGGER_FONC.info("Building url for GE and GQ");
        model.addAttribute("urlProfiler", this.dashboardConfigurationWebBean.getUrlProfiler());
        model.addAttribute("urlForms", this.dashboardConfigurationWebBean.getUrlForms());
        model.addAttribute("urlNash", this.dashboardConfigurationWebBean.getUrlNash());
        model.addAttribute("urlFeedback", this.dashboardConfigurationWebBean.getUrlFeedback());
        model.addAttribute("hostFeedback", this.dashboardConfigurationWebBean.getHostFeedback());
        model.addAttribute("urlFrontGE", this.dashboardConfigurationWebBean.getUrlFrontGE());
    }

    /**
     * Get user information to display.
     *
     * @param model
     *            The model for adding attributes
     * @param locale
     *            The browser locale
     * @return the dashboard user bean
     */
    public DashboardUserBean getUserInformation(final Model model, final Locale locale) {
        model.addAttribute("dashboardUserLanguage", locale.getLanguage());

        final DashboardUserBean dashboardUserBean = DashboardThreadContext.getUser();
        model.addAttribute("last_name", dashboardUserBean.getNom());
        model.addAttribute("first_name", dashboardUserBean.getPrenom());
        model.addAttribute("idUser", dashboardUserBean.getId());
        model.addAttribute("user", dashboardUserBean);

        LOGGER_FONC.info("Getting user information from Account with id {}", dashboardUserBean.getId());
        try {
            final AccountUserBean accountUser = this.accountFacade.readUser(dashboardUserBean.getId());
            if (null != accountUser.getConnectionDate()) {

                final LocalDateTime localDateTime = LocalDateTime.parse(accountUser.getConnectionDate());
                LOGGER_FONC.info("Last connection date {}", localDateTime.toString());

                model.addAttribute("connectionDate", FORMATTER_DATE.format(localDateTime));
                model.addAttribute("connectionHour", FORMATTER_TIME.format(localDateTime));
            }
        } catch (final Exception e) {
            LOGGER_FONC.error("An error occured when calling Account bubble", e);
        }
        return dashboardUserBean;
    }

    /**
     * Method searching GE/GQ records according to search text.
     *
     * @param dashboardRecordResult
     *            the dashboard record result to add data to
     * @param keyRecord
     *            GE for Guichet-Entreprises records. GQ for
     *            Guichet-Qualification records
     * @param userId
     *            Id user
     * @param locale
     *            Browser locale
     * @param searchTerms
     *            Searching text
     * @return Records result
     * @throws FunctionalException
     * @throws TechniqueException
     */
    public void searchRecordsFromModule(final DashboardRecordResult dashboardRecordResult, final ModulesDashboardEnum keyRecord, final Locale locale, final String searchTerms, final String sort)
            throws FunctionalException, TechniqueException {
        LOGGER_FONC.info("Search " + keyRecord + " records");
        try {
            RecordResult recordResult = null;
            final String userId = DashboardThreadContext.getUser().getId();
            // partie form1 qui doit être supprimé.
            // if (ModulesDashboardEnum.GE.equals(keyRecord)) {
            // recordResult = this.recordFormsRestService.searchRecords(userId,
            // 0, this.dashboardConfigurationWebBean.getMaxResultGEFormatted(),
            // sort, searchTerms);
            // } else
            if (ModulesDashboardEnum.GQ.equals(keyRecord)) {
                recordResult = this.recordNashRestService.searchRecords(userId, 0, this.dashboardConfigurationWebBean.getMaxResultGQFormatted(), sort, searchTerms);
            }

            if (null != recordResult) {
                LOGGER_FONC.debug("Numbers of records : {}", recordResult.getNbResults());
                recordResult = this.translate(keyRecord, userId, locale, recordResult);
                for (final RecordDisplay recordDisplay : recordResult.getRecords()) {
                    dashboardRecordResult.getRecords().add(new DashboardRecordDisplay(recordDisplay, keyRecord));
                }
                dashboardRecordResult.setNbDatasourcesOk(dashboardRecordResult.getNbDatasourcesOk() + 1);
            }
        } catch (final FunctionalException e) {
            LOGGER_FONC.error("Error occured during searching " + keyRecord + " records", e);
        } catch (final Exception e) {
            LOGGER_TECH.error("Error occured during searching " + keyRecord + " records", e);
        }
    }

    /**
     * Method searching GE/GQ records.
     *
     * @param keyRecord
     *            GE for Guichet-Entreprises records. GQ for
     *            Guichet-Qualification records
     * @param userId
     *            Id user
     * @param locale
     *            Browser locale
     * @param recordResult
     *            Records result
     */
    private RecordResult translate(final ModulesDashboardEnum keyRecord, final String userId, final Locale locale, final RecordResult recordResult) {

        final RecordResult recordResultTranslate = recordResult;
        LOGGER_FONC.info("Récupération de {} dossier(s) {} sur un total de {} pour l'utilisateur {}", recordResult.getNbResults(), keyRecord.toString(), recordResultTranslate.getTotalResults(),
                userId);
        // Definition of the labels according to the Locale of the browser
        for (final RecordDisplay record : recordResultTranslate.getRecords()) {
            // Actions
            this.translateActions(locale, record);
            // State Label
            this.translateStateLabel(locale, record);
            // Type Label
            this.translateTypeLabel(keyRecord, locale, record);
        }

        return recordResultTranslate;
    }

    /**
     * Translate Type Label.
     *
     * @param keyRecord
     *            Ge or GQ
     * @param locale
     *            local langage
     * @param record
     *            the record
     */
    private void translateTypeLabel(final ModulesDashboardEnum keyRecord, final Locale locale, final RecordDisplay record) {
        if (ModulesDashboardEnum.GE.equals(keyRecord)) {
            // Type Label
            final LocalizedLabel type = record.getType();
            if (type != null) {
                for (final LocalizedString translation : type.getValues()) {
                    if (translation.getLocale().equals(locale.getLanguage())) {
                        type.setActualLabel(translation.getValue());
                    }
                }
            }
        }
    }

    /**
     * translate State Label.
     *
     * @param locale
     *            local langage
     * @param record
     *            the record
     */
    private void translateStateLabel(final Locale locale, final RecordDisplay record) {
        final LocalizedLabel summary = record.getSummary();
        if (summary != null) {
            final List<LocalizedString> summaryValues = summary.getValues();
            if (CollectionUtils.isNotEmpty(summaryValues)) {
                for (final LocalizedString translation : summaryValues) {
                    if (translation.getLocale().equals(locale.getLanguage())) {
                        summary.setActualLabel(translation.getValue());
                    }
                }
            }
        }
    }

    /**
     * Translate Actions.
     *
     * @param locale
     *            local langage
     * @param record
     *            the record
     */
    private void translateActions(final Locale locale, final RecordDisplay record) {
        for (final RecordAction action : record.getActions()) {
            final LocalizedLabel label = action.getLabel();
            if (null != label && null != label.getValues()) {
                for (final LocalizedString translation : label.getValues()) {
                    if (translation.getLocale().equals(locale.getLanguage())) {
                        label.setActualLabel(translation.getValue());
                    }
                }
            }
        }
    }

    /**
     * Get the domain name.
     *
     * @param request
     *            The HttpServletRequest
     * @return the domain name
     */
    public String getHost(final HttpServletRequest request) {
        try {
            return new URL(request.getRequestURL().toString()).getHost();
        } catch (final MalformedURLException me) {
            LOGGER_FONC.error("An error occured when trying to get the host name");
        }
        return null;
    }

}
