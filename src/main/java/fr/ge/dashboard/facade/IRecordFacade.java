/**
 * 
 */
package fr.ge.dashboard.facade;

import fr.ge.core.exception.TechniqueException;
import fr.ge.record.ws.v1.model.RecordResult;

/**
 * Interface of the facade to call Record WS and handle the errors.
 *
 * @author $Author: amonsone $
 * @version $Revision: 0 $
 */
public interface IRecordFacade {

  /**
   * Search for records with text searching of an user, delimiting by a index and a max results. The
   * result can be ordered by create date, update date or in title alphabetical order.
   *
   * @param authorId
   *          id of the record's author
   * @param startIndex
   *          start index (>=0, by default = 0)
   * @param maxResults
   *          Number maximum of records returned. (>=1, by default = 1)
   * @param orders
   *          Possible values : creation, update, title. Default : update.
   * @param searchText
   *          text for searching records
   * @return {@link RecordResult}
   * @throws TechniqueException
   *           technique exception if the bean result is null
   */
  RecordResult searchGERecords(String authorId, int startIndex, int maxResults, String orders, String searchText)
    throws TechniqueException;

  /**
   * Search for records with text searching of an user, delimiting by a index and a max results. The
   * result can be ordered by create date, update date or in title alphabetical order.
   *
   * @param authorId
   *          id of the record's author
   * @param startIndex
   *          start index (>=0, by default = 0)
   * @param maxResults
   *          Number maximum of records returned. (>=1, by default = 1)
   * @param orders
   *          Possible values : creation, update, title. Default : update.
   * @param searchText
   *          text for searching records
   * @return {@link RecordResult}
   * @throws TechniqueException
   *           technique exception if the bean result is null
   */
  RecordResult searchGQRecords(String authorId, int startIndex, int maxResults, String orders, String searchText)
    throws TechniqueException;

}
