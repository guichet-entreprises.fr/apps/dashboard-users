/**
 *
 */
package fr.ge.dashboard.facade.impl;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import fr.ge.core.exception.TechniqueException;
import fr.ge.core.log.GestionnaireTrace;
import fr.ge.dashboard.constant.ICodeExceptionDashboardConstant;
import fr.ge.dashboard.facade.IRecordFacade;
import fr.ge.record.ws.v1.model.RecordResult;
import fr.ge.record.ws.v1.rest.IRecordDisplayRestService;

/**
 * Interface of the facade to call Record WS and handle the errors.
 *
 * @author $Author: amonsone $
 * @version $Revision: 0 $
 */
public class RecordFacadeImpl implements IRecordFacade {

    /** Interface of the Web Service Forms. */
    @Autowired
    private IRecordDisplayRestService recordFormsRestService;

    /** Interface of the Web Service Nash. */
    @Autowired
    private IRecordDisplayRestService recordNashRestService;

    /** Le logger fonctionnel. */
    private static final Logger LOGGER_FONC = GestionnaireTrace.getLoggerFonctionnel();

    /**
     * {@inheritDoc}
     *
     * @throws TechniqueException
     */
    @Override
    public RecordResult searchGERecords(final String authorId, final int startIndex, final int maxResults, final String orders, final String searchText) throws TechniqueException {
        LOGGER_FONC.debug("tri des dossiers selon {}", orders);
        RecordResult recordResultGE = null;
        try {
            recordResultGE = this.recordFormsRestService.searchRecords(authorId, startIndex, maxResults, orders, searchText);
        } catch (final Exception e) {
            final String errorFormsGE = "Erreur lors de l'appel du WS de récupération des dossiers Guichet-Entreprises pour l'utilisateur : " + authorId;
            throw new TechniqueException(ICodeExceptionDashboardConstant.ERR_DASH_WS_FORMS, errorFormsGE, e);
        }
        if (recordResultGE == null) {
            final String nullFormsGE = "Le Bean de retour est null lors de l'appel de récupération des dossiers Guichet-Entreprises pour " + "l'utilisateur : " + authorId;
            throw new TechniqueException(ICodeExceptionDashboardConstant.ERR_DASH_WS_FORMS, nullFormsGE);
        }

        return recordResultGE;
    }

    /**
     * {@inheritDoc}
     *
     * @throws TechniqueException
     */
    @Override
    public RecordResult searchGQRecords(final String authorId, final int startIndex, final int maxResults, final String orders, final String searchText) throws TechniqueException {
        LOGGER_FONC.debug("tri des dossiers selon {}", orders);
        RecordResult recordResultGQ = null;
        try {
            recordResultGQ = this.recordNashRestService.searchRecords(authorId, startIndex, maxResults, orders, searchText);
        } catch (final Exception e) {
            final String errorFormsGQ = "Erreur lors de l'appel du WS de récupération des dossiers Guichet-Qualifications pour l'utilisateur : " + authorId;
            throw new TechniqueException(ICodeExceptionDashboardConstant.ERR_DASH_WS_FORMS, errorFormsGQ, e);
        }
        if (recordResultGQ == null) {
            final String nullFormsGQ = "Le Bean de retour est null lors de l'appel de récupération des dossiers Guichet-Qualifications pour " + "l'utilisateur : " + authorId;
            throw new TechniqueException(ICodeExceptionDashboardConstant.ERR_DASH_WS_FORMS, nullFormsGQ);
        }

        return recordResultGQ;
    }

}
