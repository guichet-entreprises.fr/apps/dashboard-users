/**
 * 
 */
package fr.ge.dashboard.constant;

/**
 * Enumeration of all the modules that Dashboard call to get records
 * information.
 *
 * @author $Author: amonsone $
 * @version $Revision: 0 $
 */
public enum ModulesDashboardEnum {

    /** Module GUICHET-ENTREPRISES. */
    GE,

    /** Module GUICHET-QUALIFICATIONS. */
    GQ,

    /** Module PROFILER. */
    PROFILER;
}
