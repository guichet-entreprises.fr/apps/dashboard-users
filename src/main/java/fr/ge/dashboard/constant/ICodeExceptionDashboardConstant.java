/**
 * 
 */
package fr.ge.dashboard.constant;

/**
 * Interface ICodeExceptionDashboardConstant.
 *
 * @author $Author: amonsone $
 * @version $Revision: 0 $
 */
public interface ICodeExceptionDashboardConstant {

  /** Error code exception for the generation of the pdf to post. */
  String ERR_DASH_WS_FORMS = "ERR_DASH_WS_FORMS";

}
