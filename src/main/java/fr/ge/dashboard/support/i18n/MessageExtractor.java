/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.dashboard.support.i18n;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Enumeration;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.i18n.LocaleContextHolder;

import fr.ge.common.support.i18n.MessageReader;

/**
 * Message extractor.
 *
 * @author $Author: aakkou $
 * @version $Revision: 0 $
 */
public final class MessageExtractor {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(MessageExtractor.class);

    /**
     * Constructor.
     *
     * @param reader
     *            the reader
     */
    private MessageExtractor() {
        // Nothing to do.
    }

    /**
     * Extracts as script.
     *
     * @param reader
     *            the reader
     * @return the extraction
     */
    public static InputStream extractAsScript(final MessageReader reader) {
        final StringBuilder script = new StringBuilder();
        script.append("requirejs(['lib/i18n'], function(i18n) {\n");
        script.append("i18n.register('");
        script.append(LocaleContextHolder.getLocale().getLanguage());
        script.append("', ");
        script.append(MessageExtractor.extractAsString(reader));
        script.append(");\n");
        script.append("});");
        return new ByteArrayInputStream(script.toString().getBytes());
    }

    /**
     * Extracts as string.
     *
     * @param reader
     *            the reader
     * @return the extraction
     */
    private static String extractAsString(final MessageReader reader) {
        if (reader.getBundle() == null) {
            LOGGER.info("No extraction possible for bundle=null, key='{}'");
            return StringUtils.EMPTY;
        }
        final Enumeration<String> keys = reader.getBundle().getKeys();
        final StringBuilder script = new StringBuilder();
        script.append("{\n");
        while (keys.hasMoreElements()) {
            final String key = keys.nextElement();
            script.append("'");
            script.append(key.replaceAll("'", "\\\\'"));
            script.append("':'");
            script.append(reader.getFormatter().format(key).replaceAll("'", "\\\\'"));
            script.append("',\n");
        }
        script.append("}\n");
        return script.toString();
    }

}
