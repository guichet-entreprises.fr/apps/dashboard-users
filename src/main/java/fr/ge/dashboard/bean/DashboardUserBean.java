/**
 *
 */
package fr.ge.dashboard.bean;

import fr.ge.core.bean.UserBean;

/**
 * Bean representing an Dashboard user. XXX
 *
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public class DashboardUserBean extends UserBean {

    /** Civility. **/
    private String civilite;

    /** last name. **/
    private String nom;

    /** Fist name. **/
    private String prenom;

    /** Email address. **/
    private String email;

    /** Anonymous user. **/
    private boolean anonymous;

    /**
     * Constructeur de la classe.
     *
     * @param identifiant
     *            : identifiant de l'utilisateur
     */
    public DashboardUserBean(final String identifiant) {
        super(identifiant);
    }

    /**
     * Accesseur sur l'attribut {@link #civilite}.
     *
     * @return String civilite
     */
    public String getCivilite() {
        return this.civilite;
    }

    /**
     * Mutateur sur l'attribut {@link #civilite}.
     *
     * @param civilite
     *            la nouvelle valeur de l'attribut civilite
     */
    public DashboardUserBean setCivilite(final String civilite) {
        this.civilite = civilite;
        return this;
    }

    /**
     * Accesseur sur l'attribut {@link #nom}.
     *
     * @return String nom
     */
    public String getNom() {
        return this.nom;
    }

    /**
     * Mutateur sur l'attribut {@link #nom}.
     *
     * @param nom
     *            la nouvelle valeur de l'attribut nom
     */
    public DashboardUserBean setNom(final String nom) {
        this.nom = nom;
        return this;
    }

    /**
     * Accesseur sur l'attribut {@link #prenom}.
     *
     * @return String prenom
     */
    public String getPrenom() {
        return this.prenom;
    }

    /**
     * Mutateur sur l'attribut {@link #prenom}.
     *
     * @param prenom
     *            la nouvelle valeur de l'attribut prenom
     */
    public DashboardUserBean setPrenom(final String prenom) {
        this.prenom = prenom;
        return this;
    }

    /**
     * Accesseur sur l'attribut {@link #email}.
     *
     * @return String email
     */
    public String getEmail() {
        return this.email;
    }

    /**
     * Mutateur sur l'attribut {@link #email}.
     *
     * @param email
     *            la nouvelle valeur de l'attribut email
     */
    public DashboardUserBean setEmail(final String email) {
        this.email = email;
        return this;
    }

    @Override
    public String toString() {
        return String.format("%s %s", this.nom, this.prenom);
    }

    /**
     * Accesseur sur l'attribut {@link #anonymous}.
     *
     * @return boolean anonymous
     */
    public boolean isAnonymous() {
        return anonymous;
    }

    /**
     * Mutateur sur l'attribut {@link #anonymous}.
     *
     * @param anonymous
     *            la nouvelle valeur de l'attribut anonymous
     */
    public DashboardUserBean setAnonymous(final boolean anonymous) {
        this.anonymous = anonymous;
        return this;
    }

}
