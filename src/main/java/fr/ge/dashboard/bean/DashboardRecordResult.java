/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.dashboard.bean;

import java.util.ArrayList;
import java.util.List;

/**
 * List of records.
 *
 * @author jpauchet
 */
public class DashboardRecordResult {

    /** The list of records returned in this page. */
    private List<DashboardRecordDisplay> records;

    /** Total elements available. */
    private int totalResults;

    /** Number of datasources OK. */
    private int nbDatasourcesOk;

    /** Is at least one datasource in error ?. */
    private boolean oneDatasourceInError;

    /** Are all datasources in error ?. */
    private boolean allDatasourcesInError;

    /**
     * Constructor.
     */
    public DashboardRecordResult() {
        this.records = new ArrayList<>();
        this.totalResults = 0;
        this.nbDatasourcesOk = 0;
        this.oneDatasourceInError = false;
        this.allDatasourcesInError = false;
    }

    /**
     * Gets the records.
     *
     * @return the records
     */
    public List<DashboardRecordDisplay> getRecords() {
        return this.records;
    }

    /**
     * Sets the records.
     *
     * @param records
     *            the new records
     */
    public void setRecords(final List<DashboardRecordDisplay> records) {
        this.records = records;
    }

    /**
     * Gets the total results.
     *
     * @return the total results
     */
    public int getTotalResults() {
        return this.totalResults;
    }

    /**
     * Sets the total results.
     *
     * @param totalResults
     *            the new total results
     */
    public void setTotalResults(final int totalResults) {
        this.totalResults = totalResults;
    }

    /**
     * Gets the nb datasources ok.
     *
     * @return the nb datasources ok
     */
    public int getNbDatasourcesOk() {
        return this.nbDatasourcesOk;
    }

    /**
     * Sets the nb datasources ok.
     *
     * @param nbDatasourcesOk
     *            the new nb datasources ok
     */
    public void setNbDatasourcesOk(final int nbDatasourcesOk) {
        this.nbDatasourcesOk = nbDatasourcesOk;
    }

    /**
     * Checks if is one datasource in error.
     *
     * @return true, if is one datasource in error
     */
    public boolean isOneDatasourceInError() {
        return this.oneDatasourceInError;
    }

    /**
     * Sets the one datasource in error.
     *
     * @param oneDatasourceInError
     *            the new one datasource in error
     */
    public void setOneDatasourceInError(final boolean oneDatasourceInError) {
        this.oneDatasourceInError = oneDatasourceInError;
    }

    /**
     * Checks if is all datasources in error.
     *
     * @return true, if is all datasources in error
     */
    public boolean isAllDatasourcesInError() {
        return this.allDatasourcesInError;
    }

    /**
     * Sets the all datasources in error.
     *
     * @param allDatasourcesInError
     *            the new all datasources in error
     */
    public void setAllDatasourcesInError(final boolean allDatasourcesInError) {
        this.allDatasourcesInError = allDatasourcesInError;
    }

}
