/**
 *
 */
package fr.ge.dashboard.bean.conf;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

/**
 * Configuration class for Dashboard. TODO agréger les conf des deux
 * applications filles
 *
 * @author $Author: amonsone $
 * @version $Revision: 0 $
 */
public class DashboardConfigurationWebBean implements Serializable {

    /** UID. */
    private static final long serialVersionUID = 6702179590607766389L;

    /** The URL that check if the forms server is alive. */
    private String urlFormsServerAlive;

    /** Static instance of the DashboardConfigurationWebBean bean. */
    private static DashboardConfigurationWebBean instance = new DashboardConfigurationWebBean();

    /** The name of guichet-entreprises domain. **/
    private String geDomainName;

    /** The name of guichet-qualifications domain. **/
    private String gqDomainName;

    /** The URL for the FORMS server. */
    private String urlForms;

    /** The URL for the profiler */
    private String urlProfiler;

    /** The URL for the NASH server. */
    private String urlNash;

    /** The Front URL for guichet-entreprises. */
    private String urlFrontGE;

    /** The Front URL for guichet-qualifications. */
    private String urlFrontGQ;

    /** The URL for disconnecting from Account for GQ. */
    private String urlLogoutGQ;

    /** The for Account modification for GQ. */
    private String urlModificationGQ;

    /** The number of max results for GE. */
    private String maxResultGE;

    /** The number of max results for GQ. */
    private String maxResultGQ;

    /** The URL for the feedback user. */
    private String urlFeedback;

    /** The host for the feedback user. */
    private String hostFeedback;

    /** The URL for disconnecting from Account for GE. */
    private String urlLogoutGE;

    /** The for Account modification for GE. */
    private String urlModificationGE;

    /**
     * Return the corresponding front url for GE or GQ.
     *
     * @param hostname
     *            The host name
     * @return The url front
     */
    public String getUrlFront(final String hostname) {
        if (null != hostname && hostname.contains(this.getGqDomainName())) {
            return this.getUrlFrontGQ();
        }
        return this.getUrlFrontGE();
    }

    /**
     * Return the corresponding logout url for GE or GQ.
     *
     * @param hostname
     *            The host name
     * @return The url logout
     */
    public String getUrlLogout(final String hostname, final String currentUrl) {
        final StringBuilder finalUrl = new StringBuilder();
        if (null != hostname && hostname.contains(this.getGqDomainName())) {
            return finalUrl.append(this.getUrlLogoutGQ()).append(this.encodeURL(currentUrl)).toString();
        }
        return finalUrl.append(this.getUrlLogoutGE()).append(this.encodeURL(currentUrl)).toString();
    }

    /**
     * Return the corresponding modification url for GE or GQ.
     *
     * @param hostname
     *            The host name
     * @return The url modification
     */
    public String getUrlModification(final String hostname, final String currentUrl) {
        final StringBuilder finalUrl = new StringBuilder();
        if (null != hostname && hostname.contains(this.getGqDomainName())) {
            return finalUrl.append(this.getUrlModificationGQ()).append(this.encodeURL(currentUrl)).toString();
        }
        return finalUrl.append(this.getUrlModificationGE()).append(this.encodeURL(currentUrl)).toString();
    }

    /**
     * Encode the input URL using UTF-8 encoding.
     *
     * @param request
     *            The HttpServletRequest
     * @return The URL encoded using UTF-8 or null
     * @throws UnsupportedEncodingException
     */
    private String encodeURL(final String url) {
        try {
            return URLEncoder.encode(url, StandardCharsets.UTF_8.toString());
        } catch (final Exception e) {
            return null;
        }
    }

    /**
     * Accesseur sur l'attribut {@link #maxResultGE}.
     *
     * @return String maxResultGE
     */
    public String getMaxResultGE() {
        return this.maxResultGE;
    }

    /**
     * Accesseur sur l'attribut {@link #maxResultGE}.
     *
     * @return int maxResultGE
     */
    public int getMaxResultGEFormatted() {
        // TODO LAB : le getter ne doit jamais planter, jamais. Retourner une
        // valeur par défaut avec un
        // log, sinon refaire le set ...
        return Integer.parseInt(this.maxResultGE);
    }

    /**
     * Mutateur sur l'attribut {@link #maxResultGE}.
     *
     * @param maxResultGE
     *            la nouvelle valeur de l'attribut maxResultGE
     */
    public void setMaxResultGE(final String maxResultGE) {
        this.maxResultGE = maxResultGE;
    }

    /**
     * Accesseur sur l'attribut {@link #maxResultGQ}.
     *
     * @return String maxResultGQ
     */
    public String getMaxResultGQ() {
        return this.maxResultGQ;
    }

    /**
     * Accesseur sur l'attribut {@link #maxResultGQ}.
     *
     * @return int maxResultGQ
     */
    public int getMaxResultGQFormatted() {
        return Integer.parseInt(this.maxResultGQ);
    }

    /**
     * Mutateur sur l'attribut {@link #maxResultGQ}.
     *
     * @param maxResultGQ
     *            la nouvelle valeur de l'attribut maxResultGQ
     */
    public void setMaxResultGQ(final String maxResultGQ) {
        this.maxResultGQ = maxResultGQ;
    }

    /**
     * Accesseur sur l'attribut {@link #urlLogoutGQ}.
     *
     * @return String urlLogoutGQ
     */
    public String getUrlLogoutGQ() {
        return this.urlLogoutGQ;
    }

    /**
     * Mutateur sur l'attribut {@link #urlLogoutGQ}.
     *
     * @param urlLogoutGQ
     *            la nouvelle valeur de l'attribut urlLogoutGQ
     */
    public void setUrlLogoutGQ(final String urlLogoutGQ) {
        this.urlLogoutGQ = urlLogoutGQ;
    }

    /**
     * Accesseur sur l'attribut {@link #urlModificationGQ}.
     *
     * @return String urlModificationGQ
     */
    public String getUrlModificationGQ() {
        return this.urlModificationGQ;
    }

    /**
     * Mutateur sur l'attribut {@link #urlModificationGQ}.
     *
     * @param urlModificationGQ
     *            la nouvelle valeur de l'attribut urlModificationGQ
     */
    public void setUrlModificationGQ(final String urlModificationGQ) {
        this.urlModificationGQ = urlModificationGQ;
    }

    /**
     * Accesseur sur l'attribut {@link #urlLogoutGE}.
     *
     * @return String urlDisconnect
     */
    public String getUrlLogoutGE() {
        return this.urlLogoutGE;
    }

    /**
     * Mutateur sur l'attribut {@link #urlLogoutGE}.
     *
     * @param urlLogoutGE
     *            la nouvelle valeur de l'attribut urlLogoutGE
     */
    public void setUrlLogoutGE(final String urlLogoutGE) {
        this.urlLogoutGE = urlLogoutGE;
    }

    /**
     * Accesseur sur l'attribut {@link #urlAccount}.
     *
     * @return String urlModificationGE
     */
    public String getUrlModificationGE() {
        return this.urlModificationGE;
    }

    /**
     * Mutateur sur l'attribut {@link #urlAccount}.
     *
     * @param urlModificationGE
     *            la nouvelle valeur de l'attribut urlModificationGE
     */
    public void setUrlModificationGE(final String urlModificationGE) {
        this.urlModificationGE = urlModificationGE;
    }

    /**
     * Accesseur sur l'attribut {@link #urlFormsServer}.
     *
     * @return String urlFormsServer
     */
    public String getUrlForms() {
        return this.urlForms;
    }

    /**
     * Mutateur sur l'attribut {@link #urlForms}.
     *
     * @param urlForms
     *            la nouvelle valeur de l'attribut urlForms
     */
    public void setUrlForms(final String urlForms) {
        this.urlForms = urlForms;
    }

    /**
     * Accesseur sur l'attribut {@link #urlProfiler}.
     *
     * @return String urlProfiler
     */
    public String getUrlProfiler() {
        return this.urlProfiler;
    }

    /**
     * Mutateur sur l'attribut {@link #urlProfiler}.
     *
     * @param urlProfiler
     *            la nouvelle valeur de l'attribut urlProfiler
     */
    public void setUrlProfiler(final String urlProfiler) {
        this.urlProfiler = urlProfiler;
    }

    /**
     * Retrieves the unique instance of the DashboardConfigurationWebBean.
     *
     * @return the unique instance of the DashboardConfigurationWebBean
     */
    public static DashboardConfigurationWebBean getInstance() {
        return instance;
    }

    /**
     * Accesseur sur l'attribut {@link #urlFormsServerAlive}.
     *
     * @return String urlFormsServerAlive
     */
    public String getUrlFormsServerAlive() {
        return this.urlFormsServerAlive;
    }

    /**
     * Mutateur sur l'attribut {@link #urlFormsServerAlive}.
     *
     * @param urlFormsServerAlive
     *            la nouvelle valeur de l'attribut urlFormsServerAlive
     */
    public void setUrlFormsServerAlive(final String urlFormsServerAlive) {
        this.urlFormsServerAlive = urlFormsServerAlive;
    }

    /**
     * Accesseur sur l'attribut {@link #geDomainName}.
     *
     * @return String geDomainName
     */
    public String getGeDomainName() {
        return this.geDomainName;
    }

    /**
     * Mutateur sur l'attribut {@link #geDomainName}.
     *
     * @param geDomainName
     *            la nouvelle valeur de l'attribut geDomainName
     */
    public void setGeDomainName(final String geDomainName) {
        this.geDomainName = geDomainName;
    }

    /**
     * Accesseur sur l'attribut {@link #gqDomainName}.
     *
     * @return String gqDomainName
     */
    public String getGqDomainName() {
        return this.gqDomainName;
    }

    /**
     * Mutateur sur l'attribut {@link #gqDomainName}.
     *
     * @param gqDomainName
     *            la nouvelle valeur de l'attribut gqDomainName
     */
    public void setGqDomainName(final String gqDomainName) {
        this.gqDomainName = gqDomainName;
    }

    /**
     * Accesseur sur l'attribut {@link #urlFrontGE}.
     *
     * @return String urlFrontGE
     */
    public String getUrlFrontGE() {
        return this.urlFrontGE;
    }

    /**
     * Mutateur sur l'attribut {@link #urlFrontGE}.
     *
     * @param urlFrontGE
     *            la nouvelle valeur de l'attribut urlFrontGE
     */
    public void setUrlFrontGE(final String urlFrontGE) {
        this.urlFrontGE = urlFrontGE;
    }

    /**
     * Accesseur sur l'attribut {@link #urlFrontGQ}.
     *
     * @return String urlFrontGQ
     */
    public String getUrlFrontGQ() {
        return this.urlFrontGQ;
    }

    /**
     * Mutateur sur l'attribut {@link #urlFrontGQ}.
     *
     * @param urlFrontGQ
     *            la nouvelle valeur de l'attribut urlFrontGQ
     */
    public void setUrlFrontGQ(final String urlFrontGQ) {
        this.urlFrontGQ = urlFrontGQ;
    }

    /**
     * Accesseur sur l'attribut {@link #urlNash}.
     *
     * @return url nash
     */
    public String getUrlNash() {
        return this.urlNash;
    }

    /**
     * Mutateur sur l'attribut {@link #urlNash}.
     *
     * @param urlNash
     *            la nouvelle valeur de l'attribut urlNash
     */
    public void setUrlNash(final String urlNash) {
        this.urlNash = urlNash;
    }

    /**
     * Accesseur sur l'attribut {@link #urlFeedack}.
     *
     * @return String urlFeedack
     */
    public String getUrlFeedback() {
        return this.urlFeedback;
    }

    /**
     * Mutateur sur l'attribut {@link #urlFeedack}.
     *
     * @param urlFeedack
     *            la nouvelle valeur de l'attribut urlFeedack
     */
    public void setUrlFeedback(final String urlFeedback) {
        this.urlFeedback = urlFeedback;
    }

    /**
     * Accesseur sur l'attribut {@link #hostFeedback}.
     *
     * @return String hostFeedback
     */
    public String getHostFeedback() {
        return this.hostFeedback;
    }

    /**
     * Mutateur sur l'attribut {@link #hostFeedback}.
     *
     * @param hostFeedback
     *            la nouvelle valeur de l'attribut hostFeedback
     */
    public void setHostFeedback(final String hostFeedback) {
        this.hostFeedback = hostFeedback;
    }

}
