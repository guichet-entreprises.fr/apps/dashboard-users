package fr.ge.dashboard.interceptor;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.ws.rs.ext.Provider;

import org.apache.commons.lang3.StringUtils;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.interceptor.OutInterceptors;
import org.apache.cxf.message.Message;
import org.apache.cxf.phase.AbstractPhaseInterceptor;
import org.apache.cxf.phase.Phase;
import org.springframework.beans.factory.annotation.Value;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.ge.common.nash.ws.v1.support.feature.NashInterceptor;
import fr.ge.common.utils.CoreUtil;
import fr.ge.dashboard.bean.DashboardUserBean;
import fr.ge.dashboard.context.DashboardThreadContext;

/**
 * CXF Interceptor which modify the message to send by adding a header named
 * "X-Roles" containing user access rights.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
@Provider
@OutInterceptors
public class AclOutInterceptor extends AbstractPhaseInterceptor<Message> implements NashInterceptor<Message> {

    private static final ObjectMapper mapper = new ObjectMapper();

    @Value("${default.prefix.author:GE/}")
    private String defaultPrefixAuthor;

    public AclOutInterceptor() {
        super(Phase.PREPARE_SEND);
    }

    @Override
    public void handleMessage(final Message message) throws Fault {
        final String userId = Optional.ofNullable(DashboardThreadContext.getUser()).map(DashboardUserBean::getId).orElse(null);

        if (null != userId) {
            final Map<String, List<String>> headers = CoreUtil.cast(message.get(Message.PROTOCOL_HEADERS));
            try {
                final String roles = this.findUserRoles(userId);
                if (null != roles) {
                    headers.put("X-Roles", Arrays.asList(roles));
                }
            } catch (final IOException | RuntimeException ex) {
                throw new Fault(ex);
            }
        }
    }

    @Override
    public int getType() {
        return NashInterceptor.OUT;
    }

    /**
     * Find user roles.
     * 
     * @param userId
     *            the user identifier
     * @return
     * @throws IOException
     */
    private String findUserRoles(final String userId) throws IOException {
        final Map<String, List<String>> roles = new HashMap<>();
        roles.put(StringUtils.join(this.defaultPrefixAuthor, userId), Arrays.asList("*"));

        return mapper.writeValueAsString(roles);
    }

}
