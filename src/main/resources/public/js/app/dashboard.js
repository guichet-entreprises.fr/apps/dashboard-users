/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2014-2016)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

//requireJs loading the dependent libs
requirejs([ 'jquery', 'lib/loader', 'lib/template', 'lib/pagination', 'lib/i18n', 'jquery-ui/widgets/draggable', 'jquery-ui/widgets/droppable' ], function($, loader, Template, pagination, i18n) {

    $(function() {
    		
        /**
         * Fonction pour le bouton de suppression logique
         */
        $('a[id^="delete-index"]').on('click', function() {
            var href = $(this).attr('href'), dlg = $('#confirmRemoveModal');

            dlg.off('click').on('click', 'button[role="validate"]', function(evt) {
                dlg.modal('hide');
                loader.show();
                $.ajax(href) //
                    .done(function() {
                        window.location.reload(true);
                    }) //
                    .fail(function () {
                        loader.hide();
                        $('.modal-body', $("#singleModal")).text(i18n("The dossier was not deleted. Please try again."));
                        $("#singleModal").modal('show');
                    });
            }).modal('show');

            /** Blocking reloading of the page */
            return false;
        });
        
        /**
         * Tracker history popup
         */
        $('a[id^="history-index"]').on('click', function() {
            var href = $(this).attr('href');
            loader.show();

            $.ajax({ 
                type: 'GET', 
                url: href,
                dataType: 'json'
            })
            .done(function(response) {
            	
            	/** clear the modal body */
        		var trackerModal = $("#trackerModal");
        		$('.modal-tbody', trackerModal).empty();
        		
        		/** Parse the response and append the items */
				$.each(response, function(i, item) {
	        		$('.modal-tbody', trackerModal) //
	        			.append(
	        				'<tr class="odd">'+ //
								'<td class="message">'+item.content+'</td>'+ //
								'<td class="creation-date">'+new Date(item.created).toLocaleString()+'</td>'+ //
							'</tr>'
	            		);
				});
        		
                /** Show the modal */
                trackerModal.modal('show');
                
            }) //
            .always(function () {
                loader.hide();
            }) //
            .fail(function (response) {
            	$('.modal-title', $("#singleModal")).text(i18n("History"));
            	$('.modal-body', $("#singleModal")).text(i18n("Your log file could not be displayed. Please try again later."));
                $("#singleModal").modal('show');
            });

            /** Blocking reloading of the page */
            return false;
        });

    });

});
