// requireJs main configuration
requirejs.config({
    baseUrl : baseUrl.replace(/\/$/, '') + '/js',
    paths : {
        app : 'app',
        lib : 'lib',
        i18n : 'i18n',
        rest : '../rest'
    },
    map : {
        '*' : {
            'parsley' : 'lib/parsley'
        },
        'lib/parsley' : {
            'parsley' : 'parsleyjs'
        }
    }
});

requirejs([ 'jquery', 'rest/i18n/bundle' ], function($, i18nBundle) {

    $(function() {

        /**
         * Fonction pour le bouton de suppression dans un input
         */
        $('.has-clear input[type="text"]').on('input propertychange', function() {
            var $this = $(this);
            var visible = Boolean($this.val());
            $this.siblings('.form-control-clear').toggleClass('hidden', !visible);
        }).trigger('propertychange');

        $('.form-control-clear').click(function() {
            $(this).siblings('input[type="text"]').val('').trigger('propertychange').focus();
        });
    });

});
