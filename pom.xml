<?xml version="1.0" encoding="UTF-8"?>
<!-- Copyright SCN Guichet Entreprises, Capgemini and contributors (2014-2016) 
    This software is a computer program whose purpose is to maintain and administrate 
    standalone forms. This software is governed by the CeCILL license under French 
    law and abiding by the rules of distribution of free software. You can use, 
    modify and/ or redistribute the software under the terms of the CeCILL license 
    as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info". 
    As a counterpart to the access to the source code and rights to copy, modify 
    and redistribute granted by the license, users are provided only with a limited 
    warranty and the software's author, the holder of the economic rights, and 
    the successive licensors have only limited liability. In this respect, the 
    user's attention is drawn to the risks associated with loading, using, modifying 
    and/or developing or reproducing the software by the user in light of its 
    specific status of free software, that may mean that it is complicated to 
    manipulate, and that also therefore means that it is reserved for developers 
    and experienced professionals having in-depth computer knowledge. Users are 
    therefore encouraged to load and test the software's suitability as regards 
    their requirements in conditions enabling the security of their systems and/or 
    data to be ensured and, more generally, to use and operate it in the same 
    conditions as regards security. The fact that you are presently reading this 
    means that you have had knowledge of the CeCILL license and that you accept 
    its terms. -->
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">

    <modelVersion>4.0.0</modelVersion>

    <parent>
        <groupId>fr.ge.core</groupId>
        <artifactId>java-pom</artifactId>
        <version>2.16.1.4</version>
    </parent>

    <groupId>fr.ge.common.dashboard</groupId>
    <artifactId>ge-dashboard-webclient</artifactId>

    <version>2.16.1.6-SNAPSHOT</version>

    <packaging>war</packaging>
    <name>COMMON DASHBOARD WEBCLIENT</name>

    <properties>
        <common-utils.version>2.15.2.1</common-utils.version>
        <ge-record-ws-contract.version>2.16.1.1</ge-record-ws-contract.version>
        <ct-authentification.version>2.12.5.1</ct-authentification.version>
        <ge-i18n-thymeleaf3.version>2.7.5.2</ge-i18n-thymeleaf3.version>
        <ge-core-web.version>3.9.3.0</ge-core-web.version>
        <nash-ws-contract.version>2.16.1.8</nash-ws-contract.version>
        <layouts.version>2.16.1.2</layouts.version>
        <ge-directory-ws-contract.version>2.16.1.5</ge-directory-ws-contract.version>
        <ge-tracker-client.version>2.16.1.1</ge-tracker-client.version>
        <forms-ws-contract.version>2.12.3.0</forms-ws-contract.version>

        <webapp-target-dir>${project.build.directory}/${project.build.finalName}</webapp-target-dir>
    </properties>

    <scm>
        <connection>scm:git:https://gitlab.com/guichet-entreprises.fr/apps/dashboard-users.git</connection>
        <developerConnection>scm:git:https://gitlab.com/guichet-entreprises.fr/apps/dashboard-users.git</developerConnection>
        <url>https://gitlab.com/guichet-entreprises.fr/apps/dashboard-users</url>
        <tag>HEAD</tag>
    </scm>

    <dependencies>
        <dependency>
            <groupId>fr.ge.gent.forms</groupId>
            <artifactId>forms-ws-contract</artifactId>
            <version>${forms-ws-contract.version}</version>
        </dependency>
    	<dependency>
            <groupId>fr.ge.common.directory</groupId>
            <artifactId>ge-directory-ws-contract</artifactId>
            <version>${ge-directory-ws-contract.version}</version>
        </dependency>
        <dependency>
            <groupId>fr.ge.common.nash</groupId>
            <artifactId>nash-ws-contract</artifactId>
            <version>${nash-ws-contract.version}</version>
        </dependency>
        <dependency>
            <groupId>fr.ge.common.utils</groupId>
            <artifactId>common-web-utils</artifactId>
            <version>${common-utils.version}</version>
        </dependency>
        <dependency>
            <groupId>fr.ge.gent</groupId>
            <artifactId>ge-record-ws-contract</artifactId>
            <version>${ge-record-ws-contract.version}</version>
        </dependency>
        <dependency>
            <groupId>fr.ge.ct</groupId>
            <artifactId>ct-authentification</artifactId>
            <version>${ct-authentification.version}</version>
        </dependency>
        <dependency>
            <groupId>fr.ge.common.i18n</groupId>
            <artifactId>ge-i18n-thymeleaf3</artifactId>
            <version>${ge-i18n-thymeleaf3.version}</version>
        </dependency>
        <dependency>
            <groupId>fr.ge.core</groupId>
            <artifactId>ge-core-web</artifactId>
            <version>${ge-core-web.version}</version>
        </dependency>
        <dependency>
            <groupId>fr.ge.common.layout</groupId>
            <artifactId>nash-layout</artifactId>
            <version>${layouts.version}</version>
        </dependency>
        <dependency>
			<groupId>fr.ge.common.tracker</groupId>
			<artifactId>ge-tracker-client</artifactId>
			<version>${ge-tracker-client.version}</version>
		</dependency>
			
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-api</artifactId>
        </dependency>
        <dependency>
            <groupId>org.apache.logging.log4j</groupId>
            <artifactId>log4j-slf4j-impl</artifactId>
            <scope>runtime</scope>
        </dependency>
        <dependency>
            <groupId>org.apache.logging.log4j</groupId>
            <artifactId>log4j-api</artifactId>
            <scope>runtime</scope>
        </dependency>
        <dependency>
            <groupId>org.apache.logging.log4j</groupId>
            <artifactId>log4j-core</artifactId>
            <scope>runtime</scope>
        </dependency>

        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-webmvc</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.security</groupId>
            <artifactId>spring-security-config</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.security</groupId>
            <artifactId>spring-security-web</artifactId>
        </dependency>
        <dependency>
            <groupId>commons-fileupload</groupId>
            <artifactId>commons-fileupload</artifactId>
        </dependency>
        <dependency>
            <groupId>org.thymeleaf</groupId>
            <artifactId>thymeleaf-spring4</artifactId>
        </dependency>
        <dependency>
            <groupId>org.thymeleaf.extras</groupId>
            <artifactId>thymeleaf-extras-springsecurity4</artifactId>
        </dependency>
        <dependency>
            <groupId>org.pegdown</groupId>
            <artifactId>pegdown</artifactId>
        </dependency>
        <dependency>
            <groupId>ognl</groupId>
            <artifactId>ognl</artifactId>
        </dependency>
        <dependency>
            <groupId>net.sf.appstatus</groupId>
            <artifactId>appstatus-web</artifactId>
        </dependency>
        <dependency>
            <groupId>net.sf.appstatus</groupId>
            <artifactId>appstatus-services-inprocess</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-aop</artifactId>
        </dependency>
        <dependency>
            <groupId>org.aspectj</groupId>
            <artifactId>aspectjweaver</artifactId>
        </dependency>

        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-test</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.mockito</groupId>
            <artifactId>mockito-core</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.thymeleaf</groupId>
            <artifactId>thymeleaf-testing</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.hamcrest</groupId>
            <artifactId>hamcrest-library</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.apache.cxf</groupId>
            <artifactId>cxf-rt-rs-client</artifactId>
        </dependency>
        <!-- https://mvnrepository.com/artifact/com.fasterxml.jackson.core/jackson-core -->
        <dependency>
            <groupId>com.fasterxml.jackson.core</groupId>
            <artifactId>jackson-core</artifactId>
        </dependency>
        <dependency>
            <groupId>com.fasterxml.jackson.core</groupId>
            <artifactId>jackson-databind</artifactId>
        </dependency>
        <dependency>
            <groupId>com.fasterxml.jackson.jaxrs</groupId>
            <artifactId>jackson-jaxrs-json-provider</artifactId>
        </dependency>
        <dependency>
            <groupId>org.easytesting</groupId>
            <artifactId>fest-assert</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.apache.commons</groupId>
            <artifactId>commons-collections4</artifactId>
        </dependency>
        <dependency>
            <groupId>org.apache.commons</groupId>
            <artifactId>commons-lang3</artifactId>
        </dependency>
        <dependency>
            <groupId>commons-io</groupId>
            <artifactId>commons-io</artifactId>
        </dependency>
        <dependency>
            <groupId>javax.xml.bind</groupId>
            <artifactId>jaxb-api</artifactId>
        </dependency>
        <dependency>
            <groupId>org.parboiled</groupId>
            <artifactId>parboiled-core</artifactId>
        </dependency>
        <dependency>
            <groupId>org.parboiled</groupId>
            <artifactId>parboiled-java</artifactId>
        </dependency>
        <dependency>
            <groupId>fr.ge.common.utils</groupId>
            <artifactId>common-test-utils</artifactId>
            <version>${common-utils.version}</version>
            <scope>test</scope>
        </dependency>
    </dependencies>

    <build>
        <finalName>ge-dashboard</finalName>
        <resources>
            <resource>
                <directory>src/main/resources</directory>
                <filtering>false</filtering>
                <excludes>
                    <exclude>public/css/mixins/**</exclude>
                    <exclude>public/**/*.less</exclude>
                    <exclude>public/js/external/**</exclude>
                    <exclude>views/**/*.html</exclude>
                </excludes>
            </resource>
            <resource>
                <directory>${project.build.directory}/generated-resources</directory>
            </resource>
        </resources>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-war-plugin</artifactId>
                <configuration>
                    <packagingExcludes>css/mixins/**, **/*.less</packagingExcludes>
                </configuration>
            </plugin>
            <plugin>
                <groupId>com.github.eirslett</groupId>
                <artifactId>frontend-maven-plugin</artifactId>
                <configuration>
                    <srcdir>${basedir}/src/main/resources</srcdir>
                </configuration>
                <executions>
                    <execution>
                        <id>frontend-initialisation</id>
                        <goals>
                            <goal>install-node-and-npm</goal>
                        </goals>
                    </execution>
                    <execution>
                        <id>frontend-npm-install</id>
                        <goals>
                            <goal>npm</goal>
                        </goals>
                    </execution>
                    <execution>
                        <id>frontend-grunt</id>
                        <goals>
                            <goal>grunt</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>

    <profiles>
        <profile>
            <id>full-clean</id>
            <activation>
                <property>
                    <name>fullClean</name>
                </property>
            </activation>
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-clean-plugin</artifactId>
                        <version>3.0.0</version>
                        <configuration>
                            <filesets>
                                <fileset>
                                    <directory>${project.basedir}</directory>
                                    <includes>
                                        <include>node/**</include>
                                        <include>node_modules/**</include>
                                        <include>bower_components/**</include>
                                    </includes>
                                </fileset>
                            </filesets>
                            <followSymLinks>false</followSymLinks>
                        </configuration>
                    </plugin>
                </plugins>
            </build>
        </profile>
        <profile>
            <id>m2e</id>
            <activation>
                <property>
                    <name>m2e.version</name>
                </property>
            </activation>
            <build>
                <plugins>
                    <plugin>
                        <artifactId>maven-resources-plugin</artifactId>
                        <executions>
                            <execution>
                                <id>deploy-configuration</id>
                                <phase>generate-resources</phase>
                                <goals>
                                    <goal>copy-resources</goal>
                                </goals>
                                <configuration>
                                    <outputDirectory>${basedir}/target/classes</outputDirectory>
                                    <resources>
                                        <resource>
                                            <directory>src/main/config/</directory>
                                            <includes>
                                                <include>*.xml</include>
                                                <include>*.properties</include>
                                            </includes>
                                            <filtering>true</filtering>
                                        </resource>
                                    </resources>
                                    <encoding>UTF-8</encoding>
                                    <!-- Configuration pour hawai -->
                                    <useDefaultDelimiters>false</useDefaultDelimiters>
                                    <delimiters>
                                        <delimiter>@@*@@</delimiter>
                                    </delimiters>
                                    <filters>
                                        <filter>${basedir}/src/main/filters/ge-dashboard-webapp.properties</filter>
                                    </filters>
                                </configuration>
                            </execution>
                        </executions>
                    </plugin>
                </plugins>
            </build>
        </profile>
    </profiles>
</project>
